# Rock Tracks

## Description

This is a React front-end project using Redux and Redux tool-kit to manage state and Material UI design framework. It essentially uses the APPLE REST web service to display tracks to the user with user being able to select levels of details needed and redirects.

## How to Use

This project once started wil show a Rock music track list. The track list view displays basic information about each track but more detailed information can be obtained by using the 'Track Info' button. This displays the selected track on its own page with more details and a 'More Info' button which will redirect to the track view URL.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites

Node and NPM are required to run this appliaction and it is recommended to have the latest stable versions. For information on how to do this click [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

## Installation

1. Clone the repository using `git clone https://zzahid93@bitbucket.org/zzahid93/rock-tracks.git`
2. Run `npm install`
3. Run `npm start`

- This will then run the app in the development mode and should automatically open the browser. If not type http://localhost:3000 into a browser (Recommend Firefox or Chrome).

## Future Enhancements

1. The first enhancement I would do is to utilise the react-testing-library and build a suite of robust unit tests to ensure my code functions and to enable regression tests to be run once more features are added.
2. I would like to implement a Home page for the user to intially land on wherre the user is able to search and filter based on genre and other fields (e.g price, artist name).
3. Better feedback - Using the Loading stick in redux in the front-end using a spinner
4. Implementing error handling in the front-end.
5. Increase the quality of the front-end by adding pagination, search box integrated in the navbar, adding logo and a consistent theme.
