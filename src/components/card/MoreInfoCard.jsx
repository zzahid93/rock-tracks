import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import { useHistory, Link } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const MoreInfoCard = ({ track }) => {
  const rd = new Date(track.releaseDate);
  const releaseDate = rd.toISOString().substring(0, 10);
  const duration = msToMinsAndSecs(track.trackTimeMillis);

  const history = useHistory();
  const handleClick = () => history.push("/tracklistPage");

  function msToMinsAndSecs(ms) {
    const minutes = Math.floor(ms / 60000);
    const seconds = ((ms % 60000) / 1000).toFixed(0);
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  }

  const colors = ["#b0bec5", "#bcaaa4", "#ffab91", "#c5e1a5", "#80deea"];
  const random_color = colors[Math.floor(Math.random() * colors.length)];

  return (
    <Card
      sx={{
        display: "flex",
        minWidth: 500,
        padding: 8,
        backgroundColor: random_color,
      }}
    >
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <CardContent sx={{ flex: "1 0 auto", border: "1px solid #9e9e9e" }}>
          <Button
            variant='contained'
            color='info'
            onClick={handleClick}
            startIcon={<ArrowBackIcon />}
          >
            Back
          </Button>
          <Typography component='div' variant='h5'>
            {track.trackName}
          </Typography>
          <Typography
            variant='subtitle1'
            color='text.secondary'
            component='div'
          >
            {track.artistName}
          </Typography>
          <Divider sx={{ padding: 2 }} />

          <Typography sx={{ padding: 1 }} variant='h8' component='div'>
            {`Price: £${track.trackPrice}`}
          </Typography>
          <Typography sx={{ padding: 1 }} variant='h8' component='div'>
            {`Duration: ${duration}`}
          </Typography>
          <Typography sx={{ padding: 1 }} variant='h8' component='div'>
            {`Release: ${releaseDate}`}
          </Typography>
        </CardContent>
        <Button variant='contained'>
          <Link
            to={{ pathname: `${track.trackViewUrl}` }}
            target='_blank'
            style={{ textDecoration: "none", color: "white" }}
          >
            {" "}
            More info{" "}
          </Link>
        </Button>
      </Box>
      <CardMedia
        component='img'
        src={track.artworkUrl100}
        alt='TrackViewImages'
      />
    </Card>
  );
};

export default MoreInfoCard;
