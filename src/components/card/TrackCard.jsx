import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import { Link } from "react-router-dom";

export const TrackCard = ({ track }) => (
  <Card sx={{ maxWidth: 345 }}>
    <CardMedia
      component='img'
      src={track.artworkUrl100}
      alt='TrackViewImages'
    />
    <CardContent>
      <Typography gutterBottom variant='h5' component='div'>
        {track.trackName}
      </Typography>
      <Typography gutterBottom variant='h6' component='div'>
        {track.artistName}
      </Typography>
      <Typography gutterBottom variant='h7' component='div'>
        £{track.trackPrice}
      </Typography>
    </CardContent>
    <CardActions>
      <Button variant='contained' size='small'>
        <Link
          to={{ pathname: "/trackInfo", state: { track } }}
          style={{ textDecoration: "none", color: "white" }}
        >
          Track Info
        </Link>
      </Button>
    </CardActions>
  </Card>
);

export default TrackCard;
