import { configureStore } from '@reduxjs/toolkit';
import trackReducer from '../feature/tracks';

export const store = configureStore({
  reducer: {
    tracks: trackReducer,

  },
});

export default store;
