import {BrowserRouter as Router, Switch, Route } from 'react-router-dom'; 

import TrackListPage from './pages/TrackListPage'
import MoreDetailsPage from './pages/MoreDetailsPage';
import NavBar from './components/navBar/NavBar';

function App() {
  return (
  <Router>
    <NavBar />
    <Switch>
    <Route path='/' exact component ={TrackListPage} />
    <Route path ='/tracklistPage' exact  component={TrackListPage} />
    <Route path ='/trackInfo' component={MoreDetailsPage} />
    </Switch>
  </Router>
  );
}

export default App;