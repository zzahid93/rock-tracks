import { useEffect } from "react";

import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import { useSelector, useDispatch } from "react-redux";
import { fetchTracks, tracksSelector } from "../feature/tracks";
import TrackCard from "../components/card/TrackCard";

const TrackListPage = () => {
  const dispatch = useDispatch();
  const { tracks } = useSelector(tracksSelector);

  useEffect(() => {
    dispatch(fetchTracks());
  }, [dispatch]);

  return (
    <Paper variant='outlined' sx={{ padding: 3 }}>
      <Typography component='div' variant='h2' sx={{ paddingBottom: 5 }}>
        Track List
      </Typography>
      <Grid container spacing={4}>
        {tracks.map((track) => (
          <Grid item xs={3} key={track.trackId}>
            <TrackCard track={track} />
          </Grid>
        ))}
      </Grid>
    </Paper>
  );
};

export default TrackListPage;
