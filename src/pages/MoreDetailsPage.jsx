import Grid from "@mui/material/Grid";
import MoreInfoCard from "../components/card/MoreInfoCard";

const MoreDetailsPage = ({ location }) => {
  const { track } = location.state;

  return (
    <Grid
      container
      spacing={0}
      direction='column'
      alignItems='center'
      justify='center'
    >
      <Grid item xs={5} sx={{ padding: 10 }}>
        <MoreInfoCard track={track} />
      </Grid>
    </Grid>
  );
};

export default MoreDetailsPage;
