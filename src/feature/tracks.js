import { createSlice } from '@reduxjs/toolkit';

export const initialState = {
  loading: false,
  hasErrors: false,
  tracks: [],
};

const tracksSlice = createSlice({
  name: 'tracks',
  initialState,
  reducers: {
    getTracks: (state) => {
      state.loading = true;
    },
    getTracksSuccess: (state, { payload }) => {
      state.tracks = payload;
      state.loading = false;
      state.hasErrors = false;
    },
    getTracksFailure: (state) => {
      state.loading = false;
      state.hasErrors = true;
    },
  },
});

export const { getTracks, getTracksSuccess, getTracksFailure } = tracksSlice.actions;
export const tracksSelector = (state) => state.tracks;
export default tracksSlice.reducer;

export function fetchTracks() {
  return async (dispatch) => {
    dispatch(getTracks());

    try {
      const response = await fetch('https://itunes.apple.com/search?term=rock&country=GB&entity=song');
      const data = await response.json();

      dispatch(getTracksSuccess(data.results));
    } catch (error) {
      dispatch(getTracksFailure());
    }
  };
}